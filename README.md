# OpenML dataset: DEMOGRAPHIC-AND-SOCIO-ECONOMIC-(UNESCO)

https://www.openml.org/d/43802

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context

UNESCO: Complete set of demographic and socioeconomic variables.
UIS statistics contact: uis.datarequestsunesco.org
Sources:


United Nations, Department of Economic and Social Affairs, Population Division (2019). World Population Prospects: 2019 Revision;
For some special cases, population data are derived from Eurostat (Demographic Statistics) or National Statistical Offices;
The World Bank. World Development Indicators: April 2019;
International Monetary Fund, World Economic Outlook database: April 2019;
United Nations Statistics Division, National Accounts Main Aggregates Database (December 2018) 

Content
Variables defined:

Demographic


Fertility rate, total (births per woman)
Life expectancy at birth, total (years)
Mortality rate, infant (per 1,000 live births)
Population aged 14 years or younger 
Population aged 15-24 years 
Population aged 25-64 years 
Population aged 65 years or older 
Population growth (annual )
Prevalence of HIV, total ( of population ages 15-49)
Rural population ( of total population)
Total population


Socio-economic


Total debt service ( of GNI)
deflator (base year varies by country)
GDP growth (annual )
GDP, PPP (current international )
GDP per capita (current US)
GDP per capita, PPP (current international )
GNI per capita, Atlas method (current US)
GNI per capita, PPP (current international )
PPP conversion factor, GDP (LCU per international )
Poverty headcount ratio at 1.90 a day (PPP) ( of population)
General government total expenditure (current LCU)
GDP at market prices (constant 2010 US)
GDP (constant LCU)
GDP, PPP (constant 2011 international )
GDP per capita (current LCU)
GDP per capita, PPP (constant 2011 international )
GNI (current LCU)
GNI per capita (current LCU)
alternative conversion factor (LCU per US)
Official exchange rate (LCU per US, period average)
Price level ratio of PPP conversion factor (GDP) to market exchange rate
PPP conversion factor, private consumption (LCU per international )

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43802) of an [OpenML dataset](https://www.openml.org/d/43802). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43802/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43802/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43802/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

